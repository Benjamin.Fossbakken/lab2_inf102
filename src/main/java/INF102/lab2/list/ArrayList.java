package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

    public static final int DEFAULT_CAPACITY = 10;

    private int n;

    private Object elements[];

    public ArrayList() {
        elements = new Object[DEFAULT_CAPACITY];
    }

    @Override
    public T get(int index) {
		//hvis index er uden for listen, så kastes en IndexOutOfBoundsException
        if (index < 0 || index >= n) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
		//ellers returneres elementet på position af gitt index
        return (T) elements[index];
    }

    @Override
    public void add(int index, T element) {
		//hvis index er uden for listen, så kastes en IndexOutOfBoundsException
        if (index < 0 || index > n) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
		//Sjekker først om n er lik lengden på arrayen, dvs. om arrayen er full
        if (n == elements.length) {
            // Hvis arrayen er fyldt, så udvider vi den med det dobbelte
            int increasedLength = elements.length * 2;
            elements = Arrays.copyOf(elements, increasedLength);
        }

        // Flytter elementerne i listen en plads til højre
		// Itererer over listen og flytter elementerne fra position i-1 til i,
		// dvs. at elementerne bliver flyttet en plads til højre i listen
        for (int i = n; i > index; i--) {
            elements[i] = elements[i - 1];
        }

        //indsætter elementet på gitt index og øger størrelsen af listen
        elements[index] = element;
        n++; 
    }

    @Override
    public int size() {
        return n;
    }

    @Override
    public boolean isEmpty() {
        return n == 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder(n * 3 + 2);
        str.append("[");
        for (int i = 0; i < n; i++) {
            str.append((T) elements[i]);
            if (i != n - 1)
                str.append(", ");
        }
        str.append("]");
        return str.toString();
    }
}
