package INF102.lab2.list;

public class LinkedList<T> implements List<T> {

    private int n;

    private Node<T> head;

    @Override
    public int size() {
        return n;
    }

    @Override
    public boolean isEmpty() {
        return n == 0;
    }

	//Kalder på getNode metoden, som returnerer noden på gitt index
    @Override
    public T get(int index) {
        return getNode(index).data;
    }

    private Node<T> getNode(int index) {
		//Sjekker om inde i listen, hvis ikke kastes en IndexOutOfBoundsException
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        //Henter ut noden på gitt index
		//I hver iteration af løkken opdateres currentNode til at være den næste node i listen
        Node<T> currentNode = head;
        for (int i = 0; i < index; i++) {
            currentNode = currentNode.next;
        }
        
        return currentNode;
    }

    @Override
    public void add(int index, T element) {
		//Sjekker om inde i listen, hvis ikke kastes en IndexOutOfBoundsException
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        // Oppretter en ny node med det angitte elementet.
		// Hvis indeksen er 0, settes den nye noden som den nye hode noden.
		// Hvis indeksen ikke er 0, finner vi noden som er rett før den angitte indeksen.
        Node<T> newNode = new Node<>(element);
        if (index == 0) {
            newNode.next = head;
            head = newNode;
        } else {
            Node<T> prevNode = getNode(index - 1);
			// Den nye noden settes som den neste noden til noden som er rett før den angitte indeksen.
            newNode.next = prevNode.next;
			// Oppdaterer pekeren til noden som er rett før den angitte indeksen til å peke på den nye noden.
            prevNode.next = newNode;
        }
        
        n++;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder(n * 3 + 2);
        str.append("[");
        Node<T> currentNode = head;
        while (currentNode.next != null) {
            str.append(currentNode.data);
            str.append(", ");
            currentNode = currentNode.next;
        }
        str.append((T) currentNode.data);
        str.append("]");
        return str.toString();
    }

    private class Node<T> {
        T data;
        Node<T> next;

        public Node(T data) {
            this.data = data;
        }
    }
}
